# CLI Towers of Hanoi

This is a Command Line Interface (CLI) implementation of the Towers of Hanoi puzzle game. The Towers of Hanoi is a mathematical puzzle consisting of three towers and a number of disks of different sizes. The objective of the game is to move the entire stack of disks from one tower to another, following these rules:

1. Only one disk can be moved at a time.
2. Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack or on an empty tower.
3. No disk may be placed on top of a smaller disk.

## Features

- Interactive gameplay: The game prompts the player for input and displays the current state of the game.
- Adjustable number of disks: The player can configure the number of disks to play with, allowing for different levels of difficulty.

## Getting Started

To run the CLI Towers of Hanoi game, follow these steps:

1. Make sure you have a compatible Java Development Kit (JDK) installed on your machine.
2. Clone the repository or download the source code.
3. Compile the Java source files using the Java compiler: `javac Hanoi.java`
4. Run the game using the Java Virtual Machine: `java Hanoi`

## How to Play

1. The game will display the initial state of the puzzle, with the towers labeled as 1, 2, and 3.
2. Enter the source and destination rod for each move using the format `source-tower [.-+] destination-tower`. For example, to move a disk from tower 1 to tower 3, enter `1.3`, `1-3` or `1+3`.
3. The game will validate your move and update the state of the puzzle accordingly.
4. Continue making moves until you successfully move all the disks from the source rod to one of the other rods.
5. The game will display a congratulations message when you solve the puzzle.

## License

This CLI Towers of Hanoi game is licensed under the MIT License. Feel free to use and modify the code for your own projects.
