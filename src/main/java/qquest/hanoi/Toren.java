/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package qquest.hanoi;
import java.util.ArrayList;

/**
 *
 * @author Gebruiker
 */
public class Toren {
    
    ArrayList<Schijf> schijven;
    
    public Toren(int hoogte, int index) {
        this.schijven = new ArrayList<>();
        if (index==0) {
            for (int i=0;i<hoogte;i++) {
                this.schijven.add(new Schijf((hoogte-i)));
            }
        }
    }
    
    public void neemSchijf() {
        if (!this.schijven.isEmpty()) {
            this.schijven.remove(this.schijven.size()-1);
        }
    }
    
    public void plaatsSchijf(Schijf schijf) {
        this.schijven.add(schijf);
    }
    
    public Schijf getTopSchijf() {
        if (this.schijven.isEmpty()) {
            return null;
        } else {
            return this.schijven.get(this.schijven.size()-1);
        }
    }
}
