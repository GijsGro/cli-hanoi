/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package qquest.hanoi;
// import java.io.*;
// import javax.swing.*;
/**
 *
 * @author Gebruiker
 */

import java.util.Scanner;

public class Hanoi {

    public static void main(String[] args) {
//        JFrame frame = new JFrame();
//      JButton[] buttons = new Jbutton[3];
        
        Scanner hoogte = new Scanner(System.in);
        System.out.print("Hoogte: ");
        int n = hoogte.nextInt();
  
        Toren[] toren = new Toren[3];
        for (int i=0;i<toren.length;i++) {
//          buttons[i] = new JButton("Toren "+i);
//          buttons[i].setBounds(50+100*i,100,50,50);
            toren[i] = new Toren(n,i);
//          frame.add(buttons[i]);
        }
//        frame.setVisible(true);
        play(n, toren);
    }

    private static void play(int n, Toren[] toren) {
        /**
         * Executes the gameplay loop for the Towers of Hanoi game with the given number of disks
         *
         * @param  n       the number of disks
         * @param  toren   an array of Toren-objects
         */
        printplaatje(n,toren);
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.print("[van toren#].[naar toren#]: ");
            String userIn = input.nextLine();
            if (userIn.isEmpty()) {
                break;
            } else {
                doeZet(userIn, toren, n);
            }
            if (winConditie(toren)) {
                System.out.println("Lekker bezig kul!");
                break;
            }
        }
    }

    private static void doeZet(String userIn, Toren[] toren, int n) {
        /**
         * A method that takes user input, translates it into a pair of tower values,
         * moves the top disk of the first tower to the second tower, and prints the updated state
         * of the game
         *
         * @param  userIn  the user input as a string
         * @param  toren   an array of Toren-objects
         * @param  n      the number of disks
         * @throws NumberFormatException if the user input cannot be parsed as an integer
         */
        System.out.println();
        String[] parts = userIn.split("[.+-]");
        if (parts.length >= 2) {
            int x = Integer.parseInt(parts[0]) - 1;
            int y = Integer.parseInt(parts[1]) - 1;
            if (x >= 0 && x < 3 && y >= 0 && y < 3) {
                if (!toren[x].schijven.isEmpty() && (toren[y].schijven.isEmpty() || (toren[x].getTopSchijf().getBreedte() < toren[y].getTopSchijf().getBreedte()))) {
                    Schijf disk = toren[x].getTopSchijf();
                    toren[x].neemSchijf();
                    toren[y].plaatsSchijf(disk);
                    printplaatje(n,toren);
                }
            }
        }
    }
    
    private static void printplaatje(int n,Toren[] toren) {
        /**
         * Prints a diagram of the current state of the game
         *
         * @param  n      the number of disks
         * @param  toren  an array of Toren-objects
         */
        spaties(n+1);
        System.out.print("|");
        spaties(2*n+1);
        System.out.print("|");
        spaties(2*n+1);
        System.out.println("|");
        for (int i=0;i<n;i++) {
            for (int j=0;j<3;j++) {
                if (toren[j].schijven.isEmpty() || toren[j].schijven.size() < n-i) {
                    System.out.print(" ");
                    spaties(n);
                    System.out.print("|");
                    spaties(n);
                } else {
                    int x = toren[j].schijven.get(n-1-i).breedte;
                    System.out.print(" ");
                    spaties(n-x);
                    vakjes(x);
                    System.out.print("|");
                    vakjes(x);
                    spaties(n-x);
                }
            }
            System.out.println();
        }
        System.out.println();
    }
    
    private static void spaties(int n) {
        for (int i=0;i<n;i++) {
            System.out.print(" ");
        }
    }
    
    private static void vakjes(int n) {
        for (int i=0;i<n;i++) {
            System.out.print(n+"");
        }
    }

    private static boolean winConditie(Toren[] toren) {
        return (toren[0].schijven.isEmpty() && (toren[1].schijven.isEmpty() || toren[2].schijven.isEmpty()));
    }
}
